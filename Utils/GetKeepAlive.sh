if [ $# = 0 ]; then
  echo "Specify DEV, TEST or PROD after 'bash GetKeepAlive.sh' "
  exit 0
fi

while [ $# -gt 0 ]
do
      arg="$1"
      case "$arg" in

         TEST|-t|-test|-TEST) clear
              wget https://bitbucket.org/blwabona/atlas/raw/c0434c8b728f8dabe25f1fe40f6596a32dd870e8/Utils/KeepAlive_TEST.sh
              wget https://bitbucket.org/blwabona/atlas/raw/c0434c8b728f8dabe25f1fe40f6596a32dd870e8/Utils/ApiCall_TEST.sh
              bash KeepAlive_TEST.sh 5 m &
         ;;

         DEV|-d|-dev|-DEV) clear
               wget https://bitbucket.org/blwabona/atlas/raw/c0434c8b728f8dabe25f1fe40f6596a32dd870e8/Utils/KeepAlive_DEV.sh
               wget https://bitbucket.org/blwabona/atlas/raw/c0434c8b728f8dabe25f1fe40f6596a32dd870e8/Utils/ApiCall_DEV.sh
               bash KeepAlive_DEV.sh 5 m &
         ;;

         PROD|-p|-prod|-PROD) clear
               wget https://bitbucket.org/blwabona/atlas/raw/c0434c8b728f8dabe25f1fe40f6596a32dd870e8/Utils/KeepAlive_PROD.sh
               wget https://bitbucket.org/blwabona/atlas/raw/c0434c8b728f8dabe25f1fe40f6596a32dd870e8/Utils/ApiCall_PROD.sh
               bash KeepAlive_PROD.sh 5 m &
         ;;

         --) shift; break;;  # no more options
         *) break;; # not option, its some argument
       esac
       shift
done
