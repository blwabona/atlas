#!/bin/sh
val=$1
unit=$2
mkdir "Logs"

while true
do
  H_Accept="application/json"
  Url='localhost:8989/info'
  Now=$(date +"%F")
  #echo "current date:"  $Now
  logFile=./Logs/log_LOCAL_KeepAlive_$Now.txt
  #date + "%b-%d-%y-%H"
  date >> $logFile
  #echo "" >> $logFile
  curl -s -o /dev/null -w "%{http_code}" $Url >> $logFile
  echo "" >> $logFile
  echo "-------------------------------------------------" >> $logFile

  sleep $1$2
done
