#!/bin/sh
val=$1
unit=$2
mkdir "Logs"

while true
do
  Now=$(date +"%F")
  #echo "current date:"  $Now
  logFile=./Debug/debug_Log_$Now.txt
  #date + "%b-%d-%y-%H"
  date >> $logFile
  echo "Testing if Script continues running in background every ($1$2)" >> $logFile
  echo "" >> $logFile
  echo "-------------------------------------------------" >> $logFile

  sleep $1$2
done
