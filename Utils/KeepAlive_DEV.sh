#!/bin/sh
val=$1
unit=$2
mkdir "Logs"

if [ $# -lt 2 ]; then
  clear
  echo  'Please specify the loop interval after the script name.

(e.g. 5 minutes would be "bash KeepAlive_{ENV}.sh 5 m"),

where {ENV} = DEV, TEST or PROD

'
  exit 1
fi

while true
do
  bash ApiCall_DEV.sh
  sleep $1$2
done
