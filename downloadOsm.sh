#http://download.geofabrik.de/africa/somalia-latest.osm.pbf
#http://download.geofabrik.de/africa/south-africa-and-lesotho-latest.osm.pbf
#http://download.geofabrik.de/africa/tanzania-latest.osm.pbf

osmDataDirectory="OsmData"

if [ $# -lt 1 ]; then
  echo "Please enter the link to download the osm.pbf file:"
  read osmData;
else
  osmData=$1
fi

mkdir "$osmDataDirectory"
cd ./"$osmDataDirectory"

echo curl -O $osmData
curl -O $osmData
