#!/bin/bash
clear
navServer=$1
GH_Version=$2
OSM_Data=$3

echo $PATH_ROOT

echo "================================="
echo "Running Scripts: " "$navServer"
echo "---------------------------------"

usage=$'
**********************************
        AtlasNav - Help
**********************************

INTERACTIVE FLAGS:\n
    -d: DirectoryName - The folder in which to put all the downloaded content (e.g. OSM Data & GraphHopper) for navigation.
    -g: GH_Version - The version of GraphHopper to download (supported: '0.5' & '0.6')
    -o: OSM Data - The OSM data to be downloaded (e.g. 'south-africa-and-lesotho' will download 'south-africa-and-lesotho-latest.osm.pbf' from Geofabrik.de

DOCUMENTATION:\n
    -h: Help Menu - View this exact menu.
    -d: Does Nothing\n
**********************************'

help_d=$'
ERROR: specify the directory to be used for navigation:
try:
bash Atlas.sh -d {directoryName}
'

help_g=$'
ERROR: specify the version of graphHopper to be used (0.5 or 0.6)
try:
bash Atlas.sh -g 0.5
'

help_o=$'
ERROR: specify the osm data to be downloaded for usage:
try:
bash Atlas.sh -o south-africa-and-lesotho
'

directoryName=$1
GH_Version=$2
OSM_Data=$3

#cd ..
mkdir "$directoryName"

echo "Directory: " $directoryName;
echo "GH version: " $GH_Version
echo "OSM Data: " $OSM_Data

while [ $# -gt 0 ]
do
      arg="$1"
      case "$arg" in
         -h|--help) clear echo "$usage" ;;
         --) shift; break;;  # no more options
         *) break;; # not option, its some argument
       esac
       shift
done

while getopts ':d:g:h:o:' opt; do
    case $opt in
        d)
            directoryName=$OPTARG
            echo "Directory: " $directoryName;
            ;;
        g)
            GH_Version=$OPTARG
            echo "GH version: " $GH_Version
            ;;
        h)
            echo "-h was triggered, Parameter: $OPTARG" >&2
            ;;
        o)
            OSM_Data=$OPTARG
            echo "OSM Data: " $OSM_Data
            echo
            ;;
        \?)
          echo "Invalid option: -$OPTARG" >&2
          echo "For help try: 'bash Atlas.sh -h'"
          exit 1
          ;;
        :)
          echo "Option -$OPTARG requires an argument." >&2
          echo "For help try: 'bash Atlas.sh -h'"
          exit 1
          ;;
         -*) "-$OPTARG command not found"
			exit
			;;
    esac
done

cd ./AtlasNav
pwd

bash AtlasNavDownloadGraphHopper.sh $directoryName $GH_Version
bash AtlasNavDownloadOsmData.sh $directoryName $OSM_Data
bash AtlasNavSetupServer.sh $directoryName $GH_Version $OSM_Data
