directoryName=$1
graphHopperVersion=$2
osmData=$3
if [ $# != 3 ]; then
  echo "You need to specify 3 arguments: Server.Nav, GH_Version & OSM_Data"
  exit 1
fi

echo =========================================================
echo * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
echo 'AtlasSetupServer.sh : ' $graphHopperVersion $osmData
echo * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
echo =========================================================
cd ..
cd ~/atlas/"$directoryName"
pwd
echo ---------------------------------------------------------
echo "command: java -jar graphhopper-*.jar jetty.resources=webapp config=config-example.properties osmreader.osm=$osmData-latest.osm.pbf"
java -jar graphhopper-*.jar jetty.resources=webapp config=config-example.properties osmreader.osm=$osmData-latest.osm.pbf
