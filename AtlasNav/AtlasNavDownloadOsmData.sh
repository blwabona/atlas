#!/bin/bash
directoryName=$1
osmData=$2
echo =========================================================
echo * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
echo "AtlasDownloadOsm.sh : " "$osmData"
echo * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
echo =========================================================
cd ..
cd ./"$directoryName"
echo "Parameter Count: " $#
if [ $# -lt 2 ]; then
	echo "Too few parameters. Required: {directoryName} {osmData}"
else
	cd ./"$directoryName"
	pwd
	echo --------------------------------------------------------
	echo 'OSM Data: ' "$osmData"
	a='http://download.geofabrik.de/africa/'
	b='-latest.osm.pbf'
	#osmUrl=$a'$osmData'$b
	osmUrl="${a}${osmData}${b}"
	echo $osmUrl
	curl -O $osmUrl
fi
