#!/bin/bash
directoryName=$1
graphHopperVersion=$2
echo ========================================================
echo "ATLAS: Downloading GraphHopper Routing API v"$graphHopperVersion
echo --------------------------------------------------------
cd ..
cd ./"$directoryName"
pwd

if [ "$graphHopperVersion" == "0.5" ]; then

    if [ -f ./graphhopper-web-0.5.0-bin.zip ]; then
        echo "File already exists"
    else
        echo "Downloading v0.5"
    curl -O https://oss.sonatype.org/content/groups/public/com/graphhopper/graphhopper-web/0.5.0/graphhopper-web-0.5.0-bin.zip
    fi

else
    echo "No link for v" $graphHopperVersion
fi

#unzip the downloaded GH
unzip *.zip

rm -rf webapp
rm -rf CONTRIBUTORS.md
rm -rf NOTICE.md
rm -rf graphhopper-web-0.5.0-bin.zip

sed '' config-example.properties
sed -i '/graph.flagEncoders=car*$/ c\graph.flagEncoders=car,foot,bike' config-example.properties
