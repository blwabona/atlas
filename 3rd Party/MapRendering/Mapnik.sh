echo "http://wiki.openstreetmap.org/wiki/Mapnik"
echo "Install Instructions: https://github.com/mapnik/mapnik/wiki/Mapnik-Installation"
echo "Ubuntu Install: https://github.com/mapnik/mapnik/wiki/UbuntuInstallation"

clear

sudo apt-get update
sudo apt-get upgrade

sudo apt-get install -y python-software-properties

#Mapnik v2.2.0
sudo add-apt-repository ppa:mapnik/v2.2.0
sudo apt-get update
sudo apt-get install libmapnik libmapnik-dev mapnik-utils python-mapnik
