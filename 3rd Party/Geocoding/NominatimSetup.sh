#!/bin/bash
#http://wiki.openstreetmap.org/wiki/Nominatim/Installation

Username="nominatim"
OsmDataLink="http://download.geofabrik.de/africa/south-africa-and-lesotho-latest.osm.pbf"
OsmFileName="SouthAfrica.osm.pbf"
ApacheDocumentRoot="/var/www"

help=$'***************************************************
          NOMINATIM INSTALLATION UTIL
***************************************************

-h|-help       :  Shows this menu

-install      :  Installs Nominatim from scratch

-dependencies :  DownloadDependencies;;

-getNominatim :  DownloadNominatim;;

-getOsm       :  Downloads OSM data

-configure    :  Create Postgres Accounts, imports and indexes osm data, & setups up the website

***************************************************
'

DownloadDependencies(){
  clear
  echo ====================================================
  echo "NOMINATIM: Dependencies"
  echo ----------------------------------------------------

  sudo apt-get install build-essential libxml2-dev libgeos-dev libpq-dev libbz2-dev libtool automake libproj-dev
  sudo apt-get install libboost-dev libboost-system-dev libboost-filesystem-dev libboost-thread-dev libexpat-dev
  sudo apt-get install gcc proj-bin libgeos-c1 osmosis libgeos++-dev
  sudo apt-get install php5 php-pear php5-pgsql php5-json php-db
  sudo apt-get install postgresql postgis postgresql-contrib postgresql-9.4-postgis-2.1 postgresql-server-dev-9.4
  sudo apt-get install libprotobuf-c0-dev protobuf-c-compiler
}

DownloadFromGit(){
  clear
  echo ====================================================
  echo "NOMINATIM: Clone Nominatim from Git"
  echo ----------------------------------------------------
  sudo apt-get install git autoconf-archive

  git clone --recursive git://github.com/twain/Nominatim.git
  cd Nominatim*
  ./autogen.sh

  CompileNominatim
}

DownloadDirectly(){
  clear
  echo ====================================================
  echo "NOMINATIM: Download Nominatim Directly"
  echo ----------------------------------------------------
  curl -O http://www.nominatim.org/release/Nominatim-2.4.0.tar.bz2
  tar xvf Nominatim-2.4.0.tar.bz2
}

DownloadNominatim(){
  DownloadDirectly
}

CompileNominatim(){
  clear
  echo ====================================================
  echo "NOMINATIM: Compiling"
  echo ----------------------------------------------------
  cd Nominatim*
  pwd
  ./configure
  make
}

CustomiseInstallation(){
  clear
  echo ====================================================
  echo "NOMINATIM: Customising Installation"
  echo ----------------------------------------------------
  echo "Nothing done here. View: " http://wiki.openstreetmap.org/wiki/Nominatim/Installation
  echo ----------------------------------------------------
  echo You can customize Nominatim by creating a local configuration file settings/local.php. Have a look at settings/settings.php for available configuration settings.
  echo ----------------------------------------------------
}

CreateImporterAccount(){
  sudo -u postgres createuser -s $Username
}

CreateWebsiteUser(){
  createuser -SDR www-data
}

CreatePostgresAccounts(){
  clear
  echo ====================================================
  echo "NOMINATIM: Creating Postgres Accounts"
  echo ----------------------------------------------------

  CreateImporterAccount
  CreateWebsiteUser
}

ModuleReadingPermissions(){
  clear
  echo ====================================================
  echo "NOMINATIM: Module Reading Permissions"
  echo ----------------------------------------------------
  chmod +x ~/server.geo
  chmod +x ~/server.geo/Nominatim*
  chmod +x ~/server.geo/Nominatim*/module
}

DownloadOsmData(){
  clear
  echo ====================================================
  echo "NOMINATIM: Downloading OSM"
  echo "$OsmFileName from source: $OsmDataLink"
  echo ----------------------------------------------------
  wget --output-document $OsmFileName $OsmDataLink
}

ImportAndIndexOsmData(){
  echo ====================================================
  echo "NOMINATIM: Importing OSM"
  echo ----------------------------------------------------
  ./Nominatim*/utils/setup.php --osm-file $OsmFileName --all #| tee setup.log
}

SetupWebsite(){
  echo ====================================================
  echo "NOMINATIM: Setting Up Website"
  echo ----------------------------------------------------
  sudo mkdir -m 755 "$ApacheDocumentRoot/nominatim/"
  sudo chown "$Username" "$ApacheDocumentRoot/nominatim/"
  #sudo chown "$Username" "$ApacheDocumentRoot/nominatim/"
}

InstallNominatim(){
  DownloadDependencies
  DownloadNominatim
  CustomiseInstallation
  CreatePostgresAccounts
  ModuleReadingPermissions
  DownloadOsmData
  ImportAndIndexOsmData
  SetupWebsite
}

#InstallNominatim

while [ $# -gt 0 ]
do
      arg="$1"
      case "$arg" in
        -h|help) echo "$help" ;;
        -install) InstallNominatim ;;

        -dependencies) DownloadDependencies;;

        -getNominatim) DownloadNominatim;;

        -getOsm) DownloadOsmData ;;

        -postgres) CreatePostgresAccounts;;

        -configure)
                    CreatePostgresAccounts;
                    ModuleReadingPermissions;
                    ImportAndIndexOsmData;
                    SetupWebsite
                    ;;

         --) shift; break;;  # no more options
         *) break;; # not option, its some argument
       esac
       shift
done

#InstallNominatim
