#!/bin/bash
UserPath="/home/"$USER"/"
AtlasPath=$UserPath"atlas/"

cd "$UserPath"

# CREATE DIRECTORY FOR ATLAS COMMAND
mkdir "atlas-util"

# COPY COMMAND FILE TO DIRECTORY
cp -f "$AtlasPath""atlas" "$UserPath""atlas-util"

chmod +x $UserPath"atlas-util/atlas"

# ADD ENVIRONMENT PATH VARIABLE
echo "Add the PATH var with the command:"
echo "cat >> ~/.profile"
echo "Now enter: export PATH=$UserPath:~/atlas-util"
clear
