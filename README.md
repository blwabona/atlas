# Atlas

Atlas is a linux-based command line utility is used to manage our mapping services. In this page we cover the project structure of atlas, and the relevant scripts and commands. First things first, download atlas:

#### Project Structure

Now that atlas has been downloaded, you can see it's full project structure, where folders and scripts have the following meaning (**folders** in bold, *scripts* in italic):

- **Atlas.Nav**: contains scripts that create the navigation server `[In Progress]`
- **Atlas.Geo**: contains scripts that create the geocoding server `[Emtpy]`
- **Atlas.Map**: contains scripts that create the map-rendering server
- **OsmData**: contains OSM files used by mapping services
- **Utils**: contains files useful for various tasks
    - *downloadOsm.sh*: download OSM data and store in **OsmData**
    - *keepAlive.sh*: keep a server alive, by pinging it ever **x** seconds


- *atlas*: entry point of all commands
- ~~*configure*: configures *atlas* to be a shell command~~
- *dependencies*: installs dependencies for Atlas

## Download

- Git: `git clone https://blwabona@bitbucket.org/atlas.git`

## Configuration

Configure atlas, so you can use it as a custom terminal command:
- `cd ./atlas`
- `. configure`
  - this script will prompt you to update the `~/.profile` file with PATH variables

Done! Now you can use the `atlas` command in the terminal. The next section covers the commands.

### Commands

- `atlas -v`
      prints out the version of the currently installed Atlas.
- `atlas -h`
      prints out the documentation of Atlas.
- `atlas -osm`
      downloads the desired OSM data, where the OSM's data link is specified as a parameter after this command
- `atlas -log`
      prints the latest log file to the console.

### Navigation

- `atlas -nav`
      builds the Navigation server (**Server.Nav**)
- `atlas -rnav`
      restarts the Navigation server
- `atlas -dnav`
      deletes the Navigation server

#### Restarting the Server

- `bash ./AtlasNav/AtlasNav {Dir} {vGH} {OSM}`
    - `{Dir}`: Server.Nav
    - `{vGH}`: GraphHopper version (using `0.5` at the moment)
    - `{OSM}`: OSM Data from [GeoFabrik](http://download.geofabrik) (e.g. `south-africa-and-lesotho`)

#### Keeping the server alive

- `bash KeepAlive.sh {amount} {unit}`
    - `{unit}`: the time unit (`s`,`m`,`h`,`d` for seconds, minutes, hours, days)
    - `{amount}`: the amount of time in the given units
    - e.g.: `bash KeepAlive.sh 5 m` pings the server every 5 minutes